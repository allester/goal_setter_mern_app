# Goal Setter MERN App

This is a full stack goal setter app.

Built with the MERN stack and Redux Toolkit

## Usage

Rename the .env.example to .env and add your environment variables

    # NODE_ENV  

    # PORT  

    # MONGO_URI  

    # JWT_SECRET  
    
### Install dependencies

    # Backend dependencies
    npm install

    # Frontend dependencies
    cd frontend
    npm install

### Run Server

    # npm run server